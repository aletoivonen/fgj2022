using System;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHpBar : MonoBehaviour
{
    [Header("Settings")]
    [SerializeField] private float referenceSize;
    
    [Header("References")]
    [SerializeField] private Image fillImage;
    private Transform camera;

    private void OnEnable()
    {
        camera = Camera.main.transform;
    }

    private void Update()
    {
        Vector3 scale = Vector3.one * referenceSize * 0.01f;
        float distance = Vector3.Distance(camera.transform.position, transform.position);
        scale *= distance;
        transform.localScale = scale;
        transform.LookAt(camera);
    }

    public void SetHp(float hp)
    {
        fillImage.fillAmount = hp;
    }
}
