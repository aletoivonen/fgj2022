using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetGroupController : MonoBehaviour
{
    public static TargetGroupController Instance;

    private Cinemachine.CinemachineTargetGroup targetGroup;

    private void Awake()
    {
        Instance = this;
        targetGroup = GetComponent<Cinemachine.CinemachineTargetGroup>();
    }

    public void RegisterCameraTarget(Transform target)
    {
        targetGroup.AddMember(target, 1.0f, 1.0f);
    }

    public void UnregisterCameraTarget(Transform target)
    {
        targetGroup.RemoveMember(target);
    }
}
