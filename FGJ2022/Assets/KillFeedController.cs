using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class KillFeedController : MonoBehaviour
{
    private List<string> texts = new List<string>();
    private TMPro.TextMeshProUGUI textfield;

    // Start is called before the first frame update
    void Start()
    {
        textfield = GetComponent<TMPro.TextMeshProUGUI>();
        GameController.Instance.OnKill += OnKill;
    }

    void OnKill(int killerId, int victimId, int newScore, string weapon)
    {
        if (texts.Count >= 3)
        {
            texts.RemoveAt(0);
        }

        PlayerController killer = GameController.Instance.Players.First(p => p.Id == killerId);
        PlayerController victim = GameController.Instance.Players.First(p => p.Id == victimId);
        string killerColor = "#" + ColorUtility.ToHtmlStringRGB(killer.CharacterColor);
        string victimColor = "#" + ColorUtility.ToHtmlStringRGB(victim.CharacterColor);

        texts.Add("<color=" + killerColor + ">Player" + killerId + "</color>  <color=red>" + weapon + "</color>  <color=" + victimColor + ">Player" + victimId + "</color>");

        string all = "";
        foreach (string s in texts)
        {
            all += s + "\n";
        }
        textfield.text = all;
    }
}
