using UnityEngine;

public class CharacterSizer : MonoBehaviour
{
    [Header("References")]
    [SerializeField] private Transform headTransform;
    [SerializeField] private Transform[] legTransforms;
    [SerializeField] private Transform[] armTransforms;
    [SerializeField] private CharacterController characterController;

    [Header("Settings")]
    [SerializeField] private float headMaxSize = 3f;
    [SerializeField] private float legMaxSize = 3f;
    [SerializeField] private float handMaxSize = 2.5f;
    [SerializeField] private float characterControllerHeightMin = 2f;
    [SerializeField] private float characterControllerHeightMax = 5.8f;

    public void SetHeadSize(float normalizedSize)
    {
        headTransform.localScale = Vector3.Lerp(Vector3.one, Vector3.one * headMaxSize, normalizedSize);
    }

    public void SetLegSize(float normalizedSize)
    {
        foreach (Transform legTransform in legTransforms)
        {
            float y = Mathf.Lerp(1, legMaxSize, normalizedSize);
            legTransform.localScale = new Vector3(legTransform.localScale.x, y, legTransform.localScale.z);
        }
        characterController.height = Mathf.Lerp(characterControllerHeightMin, characterControllerHeightMax, normalizedSize);
    }

    public void SetArmSize(float normalizedSize)
    {
        foreach (Transform armTransform in armTransforms)
        {
            float y = Mathf.Lerp(1, handMaxSize, normalizedSize);
            armTransform.localScale = new Vector3(Mathf.Max(y / 2, armTransform.localScale.x), y, Mathf.Max(y / 2, armTransform.localScale.z));
        }
    }
}
