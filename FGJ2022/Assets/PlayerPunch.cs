using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerPunch : MonoBehaviour
{
    private PlayerInputReceiver playerInput;
    private Animator charAnimator;

    private float leftCooldown = 0.0f;
    private float rightCooldown = 0.0f;

    [SerializeField]
    private PlayerFist leftFist;
    [SerializeField]
    private PlayerFist rightFist;

    private void Awake()
    {
        playerInput = GetComponent<PlayerInputReceiver>();
        charAnimator = GetComponentInChildren<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (leftCooldown > 0.0f) leftCooldown -= Time.deltaTime;
        if (rightCooldown > 0.0f) rightCooldown -= Time.deltaTime;

        if (playerInput.PunchInputLeft)
        {
            var state = charAnimator.GetCurrentAnimatorStateInfo(1);
            leftFist.Punch();
            charAnimator.Play("Punch", 1);
            leftCooldown = GameController.Instance.punchCooldown + GameController.Instance.punchChargeTime;
        }
        if (playerInput.PunchInputRight)
        {
            charAnimator.Play("Punch", 2);
            rightFist.Punch();
            rightCooldown = GameController.Instance.punchCooldown + GameController.Instance.punchChargeTime;
        }
    }

    public void DisableFists()
    {
        leftFist.PunchReceived();
        rightFist.PunchReceived();
    }
}
