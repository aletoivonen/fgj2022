using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnController : MonoBehaviour
{
    public static SpawnController Instance;

    [HideInInspector]
    public List<Transform> SpawnPoints = new List<Transform>();

    private List<Transform> UnusedSpawns = new List<Transform>();

    private void Awake()
    {
        Instance = this;
        foreach (Transform t in transform)
        {
            SpawnPoints.Add(t);
        }
        ResetSpawns();
    }

    private void ResetSpawns()
    {
        UnusedSpawns = SpawnPoints;
    }

    public Vector3 UseRandomSpawn()
    {
        int index = Random.Range(0, UnusedSpawns.Count - 1);
        Vector3 pos = UnusedSpawns[index].position;
        UnusedSpawns.RemoveAt(index);
        return pos;
    }

    public Vector3 GetRandomSpawn()
    {
        int index = Random.Range(0, SpawnPoints.Count - 1);
        Vector3 pos = SpawnPoints[index].position;
        return pos;
    }

    private void OnDrawGizmosSelected()
    {
        foreach (Transform t in transform)
        {
            Gizmos.DrawCube(t.position, Vector3.one);
        }
    }
}
