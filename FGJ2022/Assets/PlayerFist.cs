using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerFist : MonoBehaviour
{
    private Collider coll;
    private PlayerController playerController;

    private void Awake()
    {
        playerController = GetComponentInParent<PlayerController>();
        coll = GetComponent<Collider>();
        coll.enabled = false;
    }

    public void Punch()
    {
        StartCoroutine(PunchCoroutine());
    }

    public int GetOwnerID()
    {
        return playerController.Id;
    }

    public void PunchReceived()
    {
        coll.enabled = false;
    }

    IEnumerator PunchCoroutine()
    {
        yield return new WaitForSeconds(GameController.Instance.punchChargeTime);
        coll.enabled = true;
        yield return new WaitForSeconds(GameController.Instance.punchCooldown);
        coll.enabled = false;
    }

}
