using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public enum SoundType
{
    DEATH,
    DMG,
    DROP,
    HIT
}
public class AudioController : MonoBehaviour
{
    public static AudioController Instance;

    [SerializeField]
    private List<AudioClip> deathSounds;
    [SerializeField]
    private List<AudioClip> dmgSounds;
    [SerializeField]
    private List<AudioClip> dropSounds;
    [SerializeField]
    private List<AudioClip> hitSounds;

    private List<AudioSource> sourcePool = new List<AudioSource>();

    [SerializeField]
    private GameObject sourcePrefab;

    public float propImpactSoundForceRequirement = 10.0f;

    private void Awake()
    {
        Instance = this;
    }

    public void PlayRandomSoundAtPos(Vector3 pos, SoundType type, float volume = -1.0f)
    {
        switch (type)
        {
            case SoundType.DEATH:
            {
                PlayRandomFromList(deathSounds, pos, volume > 0.01f ? volume : 0.8f);
                break;
            }
            case SoundType.DMG:
            {
                PlayRandomFromList(dmgSounds, pos, volume > 0.01f ? volume : 0.4f);
                break;
            }
            case SoundType.DROP:
            {
                PlayRandomFromList(dropSounds, pos, volume > 0.01f ? volume : 0.3f);
                break;
            }
            case SoundType.HIT:
            {
                PlayRandomFromList(hitSounds, pos, volume > 0.01f ? volume : 2.0f);
                break;
            }
        }
    }

    private void PlayRandomFromList(List<AudioClip> clipPool, Vector3 pos, float volume)
    {
        AudioClip clip = clipPool[Random.Range(0, clipPool.Count - 1)];
        AudioSource source;
        if (sourcePool.Count > 0)
        {
            source = sourcePool.Last();
            source.gameObject.SetActive(true);
            sourcePool.RemoveAt(sourcePool.Count - 1);
        }
        else
        {
            source = Instantiate(sourcePrefab).GetComponent<AudioSource>();
        }
        StartCoroutine(PlayClipAtPos(clip, source, pos, volume));
    }

    private IEnumerator PlayClipAtPos(AudioClip clip, AudioSource source, Vector3 pos, float volume)
    {
        source.transform.position = pos;
        source.PlayOneShot(clip, volume);
        yield return new WaitForSeconds(clip.length);
        source.gameObject.SetActive(false);
        sourcePool.Add(source);
    }
}
