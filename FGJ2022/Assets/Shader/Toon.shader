Shader "Lit/Toon"
{
    Properties
    {
        [NoScaleOffset] _MainTex("Texture", 2D) = "white" {}
        _MaximumShadow("Minimum lightness", float) = 0.8
        _DarkMultiplier("Dark multiplier", float) = 0.5
        _LightMultiplier("Light multiplier", float) = 0.5

    }
        SubShader
        {
        ZWrite On

        Pass
        {
            Tags {"LightMode" = "ForwardBase"}
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #include "UnityCG.cginc"
            #include "Lighting.cginc"

        // compile shader into multiple variants, with and without shadows
        // (we don't care about any lightmaps yet, so skip these variants)
        #pragma multi_compile_fwdbase nolightmap nodirlightmap nodynlightmap novertexlight
        // shadow helper functions and macros
        #include "AutoLight.cginc"

        struct v2f
        {
            float2 uv : TEXCOORD0;
            SHADOW_COORDS(1) // put shadows data into TEXCOORD1
            fixed3 diff : COLOR0;
            fixed3 ambient : COLOR1;
            float4 pos : SV_POSITION;
            float light : TEXCOORD2;
        };

        float inverselerp(float a, float b, float val) {
            if (a != b) {
                return clamp((val - a) / (b - a), 0, 1);
            }
            else {
                return 0.0;
            }
        }

        v2f vert(appdata_base v)
        {
            v2f o;
            o.pos = UnityObjectToClipPos(v.vertex);
            o.uv = v.texcoord;
            half3 worldNormal = UnityObjectToWorldNormal(v.normal);
            half nl = max(0, dot(worldNormal, _WorldSpaceLightPos0.xyz));
            o.diff = nl * _LightColor0.rgb;
            o.ambient = ShadeSH9(half4(worldNormal,1));
            float3 viewDir = UNITY_MATRIX_IT_MV[2].xyz;
            o.light = nl; // inverselerp(-1, 1, v.normal.y);
            //o.light = dot(worldNormal, viewDir)*2;
            // compute shadows data
            TRANSFER_SHADOW(o);
            return o;
        }

        sampler2D _MainTex;
        float _MaximumShadow;
        float _DarkMultiplier;
        float _LightMultiplier;

        fixed4 frag(v2f i) : SV_Target
        {
            fixed4 col = tex2D(_MainTex, i.uv);
            // compute shadow attenuation (1.0 = fully lit, 0.0 = fully shadowed)
            fixed shadow = max(SHADOW_ATTENUATION(i), _MaximumShadow);
            //fixed shadow = SHADOW_ATTENUATION(i);
            // darken light's illumination with shadow, keep ambient intact
            fixed3 lighting = i.diff * shadow + i.ambient;
            col.rgb *= lighting;
            col.rgb *= i.light > 0.5 ? _LightMultiplier : _DarkMultiplier;
            //return i.light;
            return col;
        }
    ENDCG
        }

// shadow casting support
UsePass "Legacy Shaders/VertexLit/SHADOWCASTER"
    }
}