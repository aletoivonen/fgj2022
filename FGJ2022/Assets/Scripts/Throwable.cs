using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(Collider))]
public class Throwable : MonoBehaviour
{
    private Rigidbody rb;
    private Transform target;
    public PlayerController Owner { get; private set; }

    public System.Action OnAttach;
    public bool IsBeingHeld { get { return target != null; } }

    public bool IsGun { get; private set; }

    private Vector3 initialPos;

    /*[SerializeField]
    private string name;*/

    private void Awake()
    {
        rb = GetComponent<Rigidbody>();
        IsGun = GetComponent<Gun>() != null;
        initialPos = transform.position;
    }

    private void Update()
    {
        if (target != null)
        {
            Vector3 diff = target.position - transform.position;
            rb.velocity = diff * ThrowableController.Instance.followStrength.Evaluate(diff.sqrMagnitude);
        }
        if (transform.position.y < GameController.Instance.minimumY)
        {
            transform.position = initialPos;
        }
    }

    public Rigidbody GetRb()
    {
        return rb;
    }

    private void Start()
    {
        ThrowableController.Instance.RegisterThrowable(this);
    }

    private void OnDestroy()
    {
        ThrowableController.Instance.UnregisterThrowable(this);
    }

    public float GetMass()
    {
        return rb.mass;
    }

    public void Attach(Transform target, PlayerController owner)
    {
        this.target = target;
        Owner = owner;
        OnAttach?.Invoke();
    }

    public void Detach()
    {
        target = null;
        StartCoroutine(DelayTag());
    }

    private IEnumerator DelayTag()
    {
        yield return new WaitForSeconds(0.07f);
        gameObject.tag = "Prop";
    }

    public void Impulse(Vector3 vec)
    {
        rb.AddForce(vec);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.CompareTag("Untagged") && collision.impulse.magnitude > AudioController.Instance.propImpactSoundForceRequirement)
            AudioController.Instance.PlayRandomSoundAtPos(transform.position, SoundType.DROP, 0.3f);
    }
}
