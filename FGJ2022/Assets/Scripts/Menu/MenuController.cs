using System;
using System.Collections;
using Cinemachine;
using TMPro;
using UnityEngine;

public class MenuController : MonoBehaviour
{
    [Header("Settings")]
    [SerializeField] private AnimationCurve countdownAnimationCurve;
    
    [Header("References")]
    [SerializeField] private MenuPlayerSlot[] playerSlots;
    [SerializeField] private CanvasGroup slotCanvasGroup;
    [SerializeField] private TextMeshProUGUI countdownText;
    [SerializeField] private CanvasGroup countdownTextCanvasGroup;
    [SerializeField] private CinemachineVirtualCamera menuCamera;

    private void OnEnable()
    {
        PlayerController.PlayerActivated += OnPlayerActivated;
        PlayerController.PlayerDisconnected += OnPlayerDisconnected;
        PlayerController.PlayerReady += OnPlayerReady;
    }
    
    private void OnDisable()
    {
        PlayerController.PlayerActivated -= OnPlayerActivated;
        PlayerController.PlayerDisconnected -= OnPlayerDisconnected;
        PlayerController.PlayerReady -= OnPlayerReady;
    }

    private IEnumerator CountdownCoroutine()
    {
        const float fadeSpeed = 1f;
        for (float time = 0; time < 1f; time += Time.deltaTime * fadeSpeed)
        {
            slotCanvasGroup.alpha = 1 - time;
            yield return null;
        }
        slotCanvasGroup.alpha = 0;
        menuCamera.Priority = 0;
        
        // Ebin countdown, does not look that good though...
        /*
        yield return new WaitForSeconds(2.5f);
        int countdown = 3;
        Vector3 textScale = Vector3.one;
        Vector3 targetScale = textScale * 1.2f;
        do
        {
            countdownText.text = countdown == 0 ? "FIGHT" : countdown.ToString();
            const float countdownSpeed = 0.75f;
            for (float time = 0; time < 1f; time += Time.deltaTime * countdownSpeed)
            {
                float fraction = countdownAnimationCurve.Evaluate(time);
                countdownText.transform.localScale = Vector3.Lerp(textScale, targetScale, fraction);
                countdownTextCanvasGroup.alpha = Mathf.Max(countdownTextCanvasGroup.alpha, time);
                yield return null;
            }
            countdownTextCanvasGroup.alpha = 1;
            countdownText.transform.localScale = Vector3.Lerp(textScale, targetScale, countdownAnimationCurve.Evaluate(1f));
            countdown--;
            yield return new WaitForSeconds(1f);
        }
        while (countdown > -1);*/

        gameObject.SetActive(false);
    }

    private void OnPlayerReady(PlayerController playerController)
    {
        for (int i = 0; i < playerSlots.Length; i++)
        {
            if (playerSlots[i].PlayerController == null)
            {
                continue;
            }

            if (!playerSlots[i].PlayerController.IsReady)
            {
                return;
            }
        }

        StartCoroutine(CountdownCoroutine());
    }

    private void OnPlayerActivated(PlayerController playerController)
    {
        for (int i = 0; i < playerSlots.Length; i++)
        {
            if (playerSlots[i].PlayerController == null)
            {
                playerSlots[i].Set(playerController);
                break;
            }
        }
    }

    private void OnPlayerDisconnected(int id)
    {
        for (int i = 0; i < playerSlots.Length; i++)
        {
            if (playerSlots[i].PlayerController.Id == id)
            {
                playerSlots[i].Clear();
                for (int j = i + 1; j < playerSlots.Length; i++)
                {
                    if (playerSlots[j].PlayerController == null)
                    {
                        playerSlots[j - 1].Clear();
                        break;
                    }
                    // Shift players back to fill empty slots in between.
                    playerSlots[j - 1].Set(playerSlots[j].PlayerController);
                }
                break;
            }
        }
    }
}
