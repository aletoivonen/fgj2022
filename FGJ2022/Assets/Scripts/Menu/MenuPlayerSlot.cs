using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class MenuPlayerSlot : MonoBehaviour
{
    public PlayerController PlayerController { get; private set; }

    [Header("References")]
    [SerializeField] private GameObject activatedArea;
    [SerializeField] private GameObject notActivatedArea;
    [SerializeField] private TextMeshProUGUI statusText;
    [SerializeField] private Button readyButton;
    [SerializeField] private Image[] handles;

    private const string waitingForPlayerText = "Press start button to connect";
    private const string playerJoinedText = "Press start to set yourself ready";
    private const string playerReadyText = "Player ready";

    private void OnDisable()
    {
        PlayerController.PlayerReady -= OnPlayerReady;
    }

    private void OnPlayerReady(PlayerController playerController)
    {
        if (PlayerController.Id == playerController.Id)
        {
            statusText.text = playerController.IsReady ? playerReadyText : playerJoinedText;
            (readyButton.targetGraphic as Image).color = playerController.IsReady ? playerController.CharacterColor : Color.white;
        }
    }
    
    public void Set(PlayerController playerController)
    {
        PlayerController = playerController;
        statusText.text = string.Format(playerJoinedText, playerController.Id);
        PlayerController.PlayerReady += OnPlayerReady;
        notActivatedArea.SetActive(false);
        activatedArea.SetActive(true);
        playerController.SetUIArea(activatedArea.transform);
        foreach (Image image in handles)
        {
            image.color = playerController.CharacterColor;
        }
    }

    public void Clear()
    {
        PlayerController = null;
        statusText.text = waitingForPlayerText;
        notActivatedArea.SetActive(true);
        activatedArea.SetActive(false);
        PlayerController.PlayerReady -= OnPlayerReady;
    }

    public void OnReady()
    {
        PlayerController.SetReady();
    }
    
    public void SetHeadSize(float normalizedSize)
    {
        if (PlayerController != null)
        {
            PlayerController.SetHeadSize(normalizedSize);
        }
    }
    
    public void SetLegSize(float normalizedSize)
    {
        if (PlayerController != null)
        {
            PlayerController.SetLegSize(normalizedSize);
        }
    }
    
    public void SetArmSize(float normalizedSize)
    {
        if (PlayerController != null)
        {
            PlayerController.SetArmSize(normalizedSize);
        }
    }
}