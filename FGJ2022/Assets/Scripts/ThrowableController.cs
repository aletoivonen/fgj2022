using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThrowableController : MonoBehaviour
{
    public static ThrowableController Instance;

    [HideInInspector]
    public List<Throwable> Throwables = new List<Throwable>();

    public AnimationCurve followStrength;

    void Awake()
    {
        Instance = this;
    }

    public void RegisterThrowable(Throwable item)
    {
        Throwables.Add(item);
    }

    public void UnregisterThrowable(Throwable item)
    {
        Throwables.Remove(item);
    }

    public Throwable GetClosest(Vector3 worldPos, float maxDist)
    {
        var near = Throwables.FindAll(item => Vector3.Distance(item.transform.position, worldPos) < maxDist);
        if (near == null || near.Count == 0)
        {
            return null;
        }
        near.Sort((a, b) =>
        {
            return Vector3.Distance(a.transform.position, worldPos).CompareTo(Vector3.Distance(b.transform.position, worldPos));
        });
        return near[0];
    }
}
