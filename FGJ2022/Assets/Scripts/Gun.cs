using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Gun : MonoBehaviour
{
    [SerializeField]
    private int damage;
    [SerializeField]
    private int projectilesPerShot = 1;
    [SerializeField]
    private int magSize;
    [SerializeField]
    private float shotCooldown;
    [SerializeField]
    private float reloadTime;
    [SerializeField]
    private float inaccuracy;
    [SerializeField]
    private Transform bulletSpawn;
    [SerializeField]
    private GameObject bulletPrefab;
    [SerializeField]
    private Transform casingSpawn;
    [SerializeField]
    private Transform muzzleSpawn;

    private int magRemaining;
    private float timeSinceShot;

    private AudioSource audioSource;
    [SerializeField]
    private List<AudioClip> fireSounds;

    private Throwable throwable;

    private LayerMask mask;

    public bool forceShoot;

    private float firerateMultiplier = 1.0f;
    private float inaccuracyMultiplier = 1.0f;
    private float projectilesMultiplier = 1.0f;

    private List<GameObject> bulletPool = new List<GameObject>();

    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
        throwable = GetComponent<Throwable>();
        magRemaining = magSize;
        audioSource = GetComponent<AudioSource>();
        throwable.OnAttach += OnAttach;
    }

    void Update()
    {
        Debug.DrawRay(bulletSpawn.position, lastDir, Color.red);
        if (throwable.IsBeingHeld || forceShoot)
        {
            timeSinceShot += Time.deltaTime;

            if (timeSinceShot > (shotCooldown * firerateMultiplier) && magRemaining > 0)
            {
                ParticlesManager.Instance.PlayGunMuzzleParticles(muzzleSpawn.position);
                ParticlesManager.Instance.PlayCasingParticles(casingSpawn.position, casingSpawn.rotation);
                Shoot();
            }
            else if (timeSinceShot > (reloadTime * firerateMultiplier))
            {
                magRemaining = magSize;
            }

            Vector3 targetDir = throwable.Owner.transform.forward;
            Vector3 dir = bulletSpawn.forward;
            Vector3 torque = Vector3.Cross(targetDir, dir);

            throwable.GetRb().angularVelocity = -torque * 0.5f;
        }
        else
        {
            timeSinceShot = -0.5f;
        }
    }

    private void OnAttach()
    {
        firerateMultiplier = GameController.Instance.BrainFirerateMultiplier.Evaluate(throwable.Owner.NormalizedHeadSize);
        inaccuracyMultiplier = GameController.Instance.BrainInaccuracyMultiplier.Evaluate(throwable.Owner.NormalizedHeadSize);
        projectilesMultiplier = GameController.Instance.BrainProjectilesMultiplier.Evaluate(throwable.Owner.NormalizedHeadSize);
    }

    private Vector3 lastDir;

    private void Shoot()
    {
        timeSinceShot = 0.0f;
        magRemaining--;
        audioSource.PlayOneShot(fireSounds[Random.Range(0, fireSounds.Count - 1)]);

        if (projectilesPerShot > 1)
        {
            for (int i = 0; i < projectilesPerShot * projectilesMultiplier; i++)
            {
                SpawnBullet();
            }
        }
        else
        {
            SpawnBullet();
        }
    }

    private void SpawnBullet()
    {
        Vector3 direction = bulletSpawn.forward;
        direction = Vector3.RotateTowards(direction, bulletSpawn.right, Random.Range(-inaccuracy, inaccuracy) * inaccuracyMultiplier * Mathf.Deg2Rad, 0.0f);
        direction = Vector3.RotateTowards(direction, bulletSpawn.up, Random.Range(-inaccuracy, inaccuracy) * inaccuracyMultiplier * Mathf.Deg2Rad, 0.0f);

        lastDir = direction;

        GameObject bullet;
        if (bulletPool.Count > 0)
        {
            bullet = bulletPool[0];
            bulletPool.RemoveAt(0);
        }
        else
        {
            bullet = Instantiate(bulletPrefab);
        }
        bullet.GetComponent<Bullet>().ShootFromPos(bulletSpawn.position, direction, throwable.Owner.Id, this, damage);
    }

    public void PoolBullet(GameObject bullet)
    {
        bulletPool.Add(bullet);
    }
}
