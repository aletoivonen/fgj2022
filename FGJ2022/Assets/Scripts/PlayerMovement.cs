using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    private CharacterController controller;
    private PlayerInputReceiver inputReceiver;
    private Vector3 playerVelocity;
    private bool groundedPlayer;
    [SerializeField]
    private float playerSpeed = 2.0f;
    [SerializeField]
    private float jumpHeight = 1.0f;
    [SerializeField]
    private float gravityValue = -9.81f;
    [SerializeField]
    private PlayerParticlesController playerParticlesController;

    private Animator charAnimator;
    private float speedMultiplier = 1.0f;
    private float jumpMultiplier = 1.0f;

    private void Awake()
    {
        charAnimator = GetComponentInChildren<Animator>();
        inputReceiver = GetComponent<PlayerInputReceiver>();
        controller = GetComponent<CharacterController>();
    }

    public void SetAlive(bool alive)
    {
        enabled = alive;
        if (!alive)
        {
            charAnimator.SetBool("Moving", false);
        }
    }

    public void SetMultipliers(float speed, float jump)
    {
        speedMultiplier = speed;
        jumpMultiplier = jump;
    }

    void Update()
    {
        if (!controller.enabled) return;

        if (!groundedPlayer && controller.isGrounded)
        {
            ParticlesManager.Instance.PlayJumpEndParticles(transform.position);
        }

        groundedPlayer = controller.isGrounded;
        if (groundedPlayer && playerVelocity.y < 0)
        {
            playerVelocity.y = 0f;
        }

        Vector3 cameraRightFlat = Camera.main.transform.right;
        Vector3 cameraForwardFlat = Camera.main.transform.forward;
        cameraRightFlat.y = 0;
        cameraForwardFlat.y = 0;
        Vector3 move = (cameraRightFlat * inputReceiver.MoveInput.x) + (cameraForwardFlat * inputReceiver.MoveInput.y); //new Vector3(inputReceiver.MoveInput.x * Camera.main.transform.right.x, 0, inputReceiver.MoveInput.y);
        controller.Move(move * Time.deltaTime * playerSpeed * speedMultiplier);

        if (move != Vector3.zero)
        {
            gameObject.transform.forward = move;
        }

        // Changes the height position of the player..
        if (inputReceiver.JumpInput && groundedPlayer)
        {
            playerVelocity.y += Mathf.Sqrt(jumpHeight * -3.0f * gravityValue) * jumpMultiplier;
            playerParticlesController.Jump();
        }

        charAnimator.SetBool("Moving", inputReceiver.MoveInput.SqrMagnitude() > 0.01f && groundedPlayer);

        playerVelocity.y += gravityValue * Time.deltaTime;
        controller.Move(playerVelocity * Time.deltaTime);
    }
}