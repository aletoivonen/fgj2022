using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerRagdoll : MonoBehaviour
{
    private List<Rigidbody> bodies;
    private Rigidbody rb;
    private CharacterController charController;
    private Animator charAnimator;

    public bool shoot = false;

    private void Awake()
    {
        bodies = new List<Rigidbody>(GetComponentsInChildren<Rigidbody>());
        rb = GetComponentInChildren<Animator>().GetComponentInChildren<Rigidbody>();
        charController = GetComponent<CharacterController>();
        charAnimator = GetComponentInChildren<Animator>();
        ToggleRagdoll(false);
    }

    public void ToggleRagdoll(bool enabled)
    {
        foreach (Rigidbody rb in bodies)
        {
            rb.isKinematic = !enabled;
        }
        charAnimator.enabled = !enabled;
        charController.enabled = !enabled;
        rb.AddForce(new Vector3(0, 5000, 0));
    }
}
