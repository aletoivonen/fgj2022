using UnityEngine;

public class JumpFollowPooledParticleSystem : PooledParticleSystem
{
    private float lastY;

    private void OnEnable()
    {
        lastY = -10000;
    }

    void Update()
    {
        if (transform.position.y < lastY)
        {
            Debug.Log("STOP");
            Stop();
        }
        lastY = transform.position.y;
    }
}
