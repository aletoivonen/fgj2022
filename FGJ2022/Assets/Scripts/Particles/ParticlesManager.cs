using System;
using System.Collections.Generic;
using UnityEngine;
using ObjectPool;

public class ParticlesManager : MonoBehaviour
{
    public static ParticlesManager Instance;
    
    [Header("References")]
    [SerializeField] private PooledParticleSystem jumpStartParticlesPrefab;
    [SerializeField] private PooledParticleSystem jumpFollowParticles;
    [SerializeField] private PooledParticleSystem gunMuzzleParticles;
    [SerializeField] private GunHitPooledParticles gunHitParticles;
    [SerializeField] private PooledParticleSystem casingParticles;
    private readonly Pool pool = new Pool();
    private readonly List<PooledParticleSystem> instantiatedParticles = new List<PooledParticleSystem>();

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
        
        pool.PopulatePool(jumpStartParticlesPrefab, 10, transform);
        pool.PopulatePool(jumpFollowParticles, 10, transform);
        pool.PopulatePool(gunMuzzleParticles, 20, transform);
        pool.PopulatePool(gunHitParticles, 20, transform);
        pool.PopulatePool(casingParticles, 20, transform);
    }

    private void LateUpdate()
    {
        for (int i = instantiatedParticles.Count - 1; i > -1; i--)
        {
            if (!instantiatedParticles[i].IsPlaying)
            {
                pool.Destroy(instantiatedParticles[i]);
                instantiatedParticles.RemoveAt(i);
            }
        }
    }

    public void PlayJumpParticles(Vector3 position, Transform parent)
    {
        instantiatedParticles.Add(pool.Instantiate<PooledParticleSystem>(jumpStartParticlesPrefab, position, jumpStartParticlesPrefab.transform.rotation));
        instantiatedParticles.Add(pool.Instantiate<PooledParticleSystem>(jumpFollowParticles, position, Quaternion.identity, parent));
    }
    
    public void PlayJumpEndParticles(Vector3 position)
    {
        instantiatedParticles.Add(pool.Instantiate<PooledParticleSystem>(jumpStartParticlesPrefab, position, jumpStartParticlesPrefab.transform.rotation, transform));
    }

    public void PlayGunMuzzleParticles(Vector3 position)
    {
        instantiatedParticles.Add(pool.Instantiate<PooledParticleSystem>(gunMuzzleParticles, position, Quaternion.identity));
    }

    public void PlayGunHitParticles(Vector3 position, Quaternion rotation, Color color)
    {
        GunHitPooledParticles particles = pool.Instantiate<GunHitPooledParticles>(gunHitParticles, position, rotation);
        particles.SetColor(color);
        instantiatedParticles.Add(particles);
    }

    public void PlayCasingParticles(Vector3 position, Quaternion rotation)
    {
        instantiatedParticles.Add(pool.Instantiate<PooledParticleSystem>(casingParticles, position, rotation));
    }
}
