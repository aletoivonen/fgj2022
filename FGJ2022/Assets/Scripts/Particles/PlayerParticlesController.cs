using System;
using UnityEngine;

public class PlayerParticlesController : MonoBehaviour
{
    [Header("References")]
    [SerializeField] private Transform bottom;
    [SerializeField] private Transform rightStep;
    [SerializeField] private Transform leftStep;

    public void Jump()
    {
        ParticlesManager.Instance.PlayJumpParticles(bottom.transform.position, transform);
    }

    public void StepRight()
    {
        // Add step interaction if you want
    }

    public void StepLeft()
    {
        // Add step interaction if you want
    }
}
