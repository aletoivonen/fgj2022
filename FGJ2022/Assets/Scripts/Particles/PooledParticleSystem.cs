using System.Linq;
using UnityEngine;
using ObjectPool;

public class PooledParticleSystem : MonoBehaviour, IPoolable
{
    public bool IsPlaying => particleSystems.Any(x => x.isPlaying);
    
    [Header("References")]
    [SerializeField] private ParticleSystem[] particleSystems;
    
#if UNITY_EDITOR
    private void OnValidate()
    {
        if (!Application.isPlaying && GetComponent<ParticleSystem>() == null)
        {
            particleSystems = GetComponentsInChildren<ParticleSystem>();
        }
    }
#endif

    protected void Stop()
    {
        foreach (ParticleSystem particleSystem in particleSystems)
        {
            particleSystem.Stop();
        }
    }
    
    public void OnInstantiated()
    {
        gameObject.SetActive(true);
        particleSystems[0].Play(true);
    }

    public void OnDestroyed()
    {
        gameObject.SetActive(false);
    }

    public void OnPopulated()
    {
        gameObject.SetActive(false);
    }
}
