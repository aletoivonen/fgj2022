using UnityEngine;

public class GunHitPooledParticles : PooledParticleSystem
{
    [Header("References")]
    [SerializeField] private ParticleSystem streakParticleSystem;
    
    public void SetColor(Color color)
    {
        ParticleSystem.MainModule mainModule = streakParticleSystem.main;
        mainModule.startColor = color;
    }
}
