using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{
    public static GameController Instance;

    public List<PlayerController> Players;
    public Canvas EndMenu;

    public AnimationCurve CollisionDamageRatio;

    public float RespawnTime = 3.0f;
    public int MaxHealth = 20; // TEMP move to player balance thingy
    public int MaxRespawns = 3;

    public float minimumY = -5f;

    public float punchChargeTime = 0.05f;
    public float punchCooldown = 0.05f;

    public AnimationCurve BrainFirerateMultiplier;
    public AnimationCurve BrainInaccuracyMultiplier;
    public AnimationCurve BrainProjectilesMultiplier;
    public AnimationCurve LegsSpeedMultiplier;
    public AnimationCurve LegsJumpMultiplier;
    public AnimationCurve ArmsStrengthMultiplier;

    private Dictionary<int, int> playerScore = new Dictionary<int, int>();

    //Killer, victim, new score, weapon
    public System.Action<int, int, int, string> OnKill;

    private void Awake()
    {
        Instance = this;
    }

    public void RegisterPlayer(PlayerController player)
    {
        Players.Add(player);
    }

    public void UnregisterPlayer(PlayerController player)
    {
        Players.Remove(player);
    }

    public void AddKill(int victim, int killer, string weapon)
    {
        if (!playerScore.ContainsKey(killer)) playerScore[killer] = 0;
        playerScore[killer] += killer != victim ? 1 : -1;
        OnKill?.Invoke(killer, victim, playerScore[killer], weapon);
        foreach (KeyValuePair<int, int> pair in playerScore)
        {
            Debug.Log("player " + pair.Key + " has " + pair.Value + " score");
        }
    }

    public void EndGame()
    {
        PlayerController winningPlayer = null;
        foreach (PlayerController playerController in Players)
        {
            playerController.SetSelectedUI(EndMenu.GetComponentInChildren<Button>());
            if (playerController.PlayerHealth.CurrentHealth > 0)
            {
                winningPlayer = playerController;
            }
        }
        if (winningPlayer != null)
        {
            EndMenu.GetComponentInChildren<TextMeshProUGUI>().text =
                $"Game Over!{System.Environment.NewLine}{winningPlayer.Name}Won the game!";
        }
        else
        {
            EndMenu.GetComponentInChildren<TextMeshProUGUI>().text =
                $"Game Over!{System.Environment.NewLine}No winners this game!";
        }

        IEnumerator endCoroutine()
        {
            EndMenu.GetComponentInChildren<Button>().interactable = false;
            EndMenu.gameObject.SetActive(true);
            yield return new WaitForSeconds(2f);
            EndMenu.GetComponentInChildren<Button>().interactable = true;
        }
        StartCoroutine(endCoroutine());
    }

    public void Restart()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene(0);
    }
}
