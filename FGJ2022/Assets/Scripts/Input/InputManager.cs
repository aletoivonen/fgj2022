using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class InputManager : MonoBehaviour
{
    [Header("References")]
    [SerializeField] private GameObject playerInputPrefab;
    [SerializeField] private List<string> allowedDeviceTypes;

    public static InputManager Instance;
    public readonly Dictionary<int, PlayerController> devices = new Dictionary<int, PlayerController>();

    // Lazy singleton pattern
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
            return;
        }

        foreach (InputDevice device in InputSystem.devices)
        {
            AddDevice(device);
        }
        InputSystem.onDeviceChange += OnInputDeviceChange;
    }

    private void OnDestroy()
    {
        InputSystem.onDeviceChange -= OnInputDeviceChange;
    }

    private void AddDevice(InputDevice device)
    {
        if (!devices.ContainsKey(device.deviceId) && allowedDeviceTypes.Contains(device.description.interfaceName))
        {
            PlayerInput playerInput = PlayerInput.Instantiate(playerInputPrefab, -1, "Gamepad", -1, device);
            PlayerController addedPlayerController = playerInput.GetComponent<PlayerController>();
            addedPlayerController.Initialize(device);
            devices.Add(device.deviceId, addedPlayerController);
        }
    }

    private void RemoveDevice(InputDevice device)
    {
        if (devices.TryGetValue(device.deviceId, out PlayerController removedPlayerController))
        {
            devices[device.deviceId].TearDown();
            Destroy(removedPlayerController.gameObject);
            devices.Remove(device.deviceId);
        }
    }

    public void OnInputDeviceChange(InputDevice device, InputDeviceChange change)
    {
        switch (change)
        {
            case InputDeviceChange.Added:
                AddDevice(device);
                break;
            case InputDeviceChange.Disconnected:
                RemoveDevice(device);
                break;
        }
    }
}
