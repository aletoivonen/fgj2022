using System;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.UI;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    [Header("References")]
    [SerializeField] private PlayerInput playerInput;
    [SerializeField] private MultiplayerEventSystem multiplayerEventSystem;
    [SerializeField] private CharacterSizer characterSizer;
    [SerializeField] private Material[] characterMaterials;
    [SerializeField] private Color[] characterColors;
    [SerializeField] private SkinnedMeshRenderer skinnedMeshRenderer;
    [SerializeField] private PlayerHealth playerHealth;

    public static Action<int> PlayerJoined;
    public static Action<int> PlayerDisconnected;
    public static Action<PlayerController> PlayerActivated;
    public static Action<PlayerController> PlayerReady;

    public string Name => $"Player {playerInput.playerIndex}";
    public PlayerHealth PlayerHealth => playerHealth;
    public int Id { get; private set; }
    public bool IsReady { get; private set; }
    public Color CharacterColor { get; private set; }

    private bool isActivated;

    public float NormalizedHeadSize { get; private set; }
    public float NormalizedArmSize { get; private set; }
    public float NormalizedLegSize { get; private set; }

    public void OnActivatePlayer()
    {
        if (!isActivated)
        {
            isActivated = true;
            if (PlayerActivated != null)
            {
                PlayerActivated(this);
            }
        }
    }

    public void Initialize(InputDevice device)
    {
        gameObject.name = $"Player Input {device.name} {device.deviceId}";
        Id = device.deviceId;
        if (PlayerJoined != null)
        {
            PlayerJoined(Id);
        }
    }

    public void TearDown()
    {
        if (PlayerDisconnected != null)
        {
            PlayerDisconnected(Id);
        }
    }

    private void Start()
    {
        GameController.Instance.RegisterPlayer(this);
        TargetGroupController.Instance.RegisterCameraTarget(transform);
        transform.position = SpawnController.Instance.UseRandomSpawn();
        Material material = characterMaterials[Mathf.Min(playerInput.playerIndex, characterMaterials.Length - 1)];
        skinnedMeshRenderer.sharedMaterial = material;
        CharacterColor = characterColors[Mathf.Min(playerInput.playerIndex, characterColors.Length - 1)];

        SetArmSize(0);
        SetHeadSize(0);
        SetLegSize(0);
    }

    private void OnDestroy()
    {
        GameController.Instance.UnregisterPlayer(this);
        TargetGroupController.Instance.UnregisterCameraTarget(transform);
    }

    public void SetUIArea(Transform root)
    {
        multiplayerEventSystem.playerRoot = root.gameObject;
        multiplayerEventSystem.firstSelectedGameObject = root.GetChild(1).gameObject;
        multiplayerEventSystem.SetSelectedGameObject(root.GetChild(1).gameObject);
    }

    public void SetReady()
    {
        IsReady = !IsReady;
        if (PlayerReady != null)
        {
            PlayerReady(this);
        }
    }

    public void SetHeadSize(float normalizedSize)
    {
        NormalizedHeadSize = normalizedSize;
        characterSizer.SetHeadSize(normalizedSize);
    }

    public void SetLegSize(float normalizedSize)
    {
        NormalizedLegSize = normalizedSize;
        characterSizer.SetLegSize(normalizedSize);
        GetComponent<PlayerMovement>().SetMultipliers(
            GameController.Instance.LegsSpeedMultiplier.Evaluate(normalizedSize),
            GameController.Instance.LegsJumpMultiplier.Evaluate(normalizedSize)
            );
    }

    public void SetSelectedUI(Selectable selectable)
    {
        multiplayerEventSystem.SetSelectedGameObject(selectable.gameObject);
    }

    public void SetArmSize(float normalizedSize)
    {
        NormalizedArmSize = normalizedSize;
        characterSizer.SetArmSize(normalizedSize);
    }
}
