using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHealth : MonoBehaviour
{
    public int MaxRespawns;
    public int CurrentRespawns;

    public int MaxHealth;
    public int CurrentHealth;
    public PlayerHpBar PlayerHpBar;

    private PlayerRagdoll ragdoll;
    private PlayerThrow thrower;
    private PlayerMovement movement;
    private PlayerController playerController;

    public bool die;

    private void Awake()
    {
        ragdoll = GetComponent<PlayerRagdoll>();
        thrower = GetComponent<PlayerThrow>();
        movement = GetComponent<PlayerMovement>();
        playerController = GetComponent<PlayerController>();
    }

    private void Start()
    {
        var hitAreas = GetComponentsInChildren<PlayerHitArea>();
        foreach (PlayerHitArea h in hitAreas)
        {
            h.OnTakeDamage += TakeDamage;
            h.SetPlayerController(playerController);
        }
        var cols = GetComponentsInChildren<Collider>();
        foreach (Collider col in cols)
        {
            foreach (Collider c in cols)
            {
                if (col != c)
                {
                    Physics.IgnoreCollision(col, c);
                }
            }
        }
        MaxHealth = GameController.Instance.MaxHealth;
        MaxRespawns = GameController.Instance.MaxRespawns;
        CurrentHealth = MaxHealth;
        CurrentRespawns = MaxRespawns;
    }

    private void Update()
    {
        if (die)
        {
            Die();
            die = false;
        }

        if (CurrentHealth > 0 && transform.position.y < GameController.Instance.minimumY)
        {
            Die();
            GameController.Instance.AddKill(playerController.Id, playerController.Id, "small brain");
        }
    }

    private void TakeDamage(int damage, int player, string weapon)
    {
        if (CurrentHealth <= 0) return;

        CurrentHealth -= damage;
        PlayerHpBar.SetHp(CurrentHealth / (float)MaxHealth);
        if (CurrentHealth <= 0)
        {
            Die();
            GameController.Instance.AddKill(playerController.Id, player, weapon);
        }
        else if (Random.value > 0.5f)
        {
            AudioController.Instance.PlayRandomSoundAtPos(transform.position, SoundType.DMG);
        }
    }

    private void Die()
    {
        CurrentHealth = 0;
        AudioController.Instance.PlayRandomSoundAtPos(transform.position, SoundType.DEATH);
        ragdoll.ToggleRagdoll(true);
        movement.SetAlive(false);
        GetComponent<PlayerPunch>().DisableFists();
        thrower.ForceRelease();
        thrower.enabled = false;
        if (CurrentRespawns > 0)
        {
            CurrentRespawns--;
            StartCoroutine(Respawn());
        }
        else
        {
            int amountAlive = 0;
            foreach (PlayerController playerController in GameController.Instance.Players)
            {
                if (playerController.PlayerHealth.CurrentRespawns > 0)
                {
                    amountAlive++;
                }
            }

            if (amountAlive < 2)
            {
                GameController.Instance.EndGame();
            }
        }
    }

    private IEnumerator Respawn()
    {
        yield return new WaitForSecondsRealtime(3.0f);
        movement.SetAlive(true);
        thrower.enabled = true;
        transform.position = SpawnController.Instance.GetRandomSpawn();
        ragdoll.ToggleRagdoll(false);
        CurrentHealth = MaxHealth;
        PlayerHpBar.SetHp(CurrentHealth / (float)MaxHealth);
    }
}
