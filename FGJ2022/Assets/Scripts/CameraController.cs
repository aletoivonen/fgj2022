using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public static CameraController Instance;

    [SerializeField]
    private Vector3 offset;
    [SerializeField]
    private float lerp = 0.9f;

    private Vector3 playersMin;
    private Vector3 playersMax;

    private void Awake()
    {
        Instance = this;
    }

    private void Update()
    {
        if (GameController.Instance.Players.Count > 0)
        {
            playersMin = new Vector3(1000, 0, 1000);
            playersMax = new Vector3(-1000, 0, -1000);
            foreach (var p in GameController.Instance.Players)
            {
                if (p.transform.position.x < playersMin.x) playersMin.x = p.transform.position.x;
                if (p.transform.position.z < playersMin.z) playersMin.z = p.transform.position.z;
                if (p.transform.position.x > playersMax.x) playersMax.x = p.transform.position.x;
                if (p.transform.position.z > playersMax.z) playersMax.z = p.transform.position.z;
            }
        }

        Vector3 middle = playersMin + (playersMax - playersMin) / 2;
        transform.position = Vector3.Lerp(transform.position, middle + offset, lerp);
    }
}
