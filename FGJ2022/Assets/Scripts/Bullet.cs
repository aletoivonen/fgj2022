using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    [SerializeField]
    private Rigidbody rb;
    [SerializeField]
    private float speed;
    [SerializeField]
    private Collider coll;
    [SerializeField]
    private Color defaultHitColor;
    public Gun OriginGun { get; private set; }

    private bool removing;

    public int Damage { get; private set; }

    float flyTime = 0.0f;

    public int shooterId { get; private set; }

    public void ShootFromPos(Vector3 position, Vector3 direction, int shooterId, Gun gun, int damage)
    {
        this.OriginGun = gun;
        coll.enabled = true;
        gameObject.SetActive(true);
        transform.position = position;
        transform.forward = direction;
        rb.AddForce(direction * speed);
        this.shooterId = shooterId;
        Damage = damage;
    }

    private void Update()
    {
        flyTime += Time.deltaTime;
        if (flyTime > 3.0f && !removing)
        {
            Remove();
        }
    }

    private void Remove()
    {
        StartCoroutine(RemoveBullet());
    }

    private void OnCollisionEnter(Collision collision)
    {
        Remove();
        Vector3 position = collision.contacts[0].point;
        Quaternion rotation = Quaternion.LookRotation(collision.contacts[0].normal);
        ParticlesManager.Instance.PlayGunHitParticles(position, rotation, defaultHitColor);
    }
    private void OnTriggerEnter(Collider coll)
    {
        Remove();
    }

    private IEnumerator RemoveBullet()
    {
        if (removing) yield break;
        removing = true;
        rb.isKinematic = true;
        coll.enabled = false;
        yield return new WaitForSeconds(0.1f);
        OriginGun.PoolBullet(gameObject);
        rb.isKinematic = false;
        flyTime = 0.0f;
        removing = false;
        gameObject.SetActive(false);
    }
}
