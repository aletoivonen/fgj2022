using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider))]
public class PlayerHitArea : MonoBehaviour
{
    public float damageMultiplier;

    [HideInInspector]
    public Collider coll;

    public System.Action<int, int, string> OnTakeDamage;

    private PlayerThrow thrower;
    private PlayerController playerController;

    private void Awake()
    {
        thrower = GetComponentInParent<PlayerThrow>();
        coll = GetComponent<Collider>();
    }

    public void SetPlayerController(PlayerController p)
    {
        playerController = p;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.CompareTag("Prop"))
        {
            AudioController.Instance.PlayRandomSoundAtPos(transform.position, SoundType.HIT);
            collision.collider.gameObject.tag = "Untagged";
            if (thrower.CompareToTargets(collision.collider.gameObject)) return;
            int damage = Mathf.RoundToInt(GameController.Instance.CollisionDamageRatio.Evaluate(collision.impulse.sqrMagnitude));
            Throwable throwable = collision.collider.GetComponent<Throwable>();
            int player = throwable.Owner.Id;
            if (damage > 0 && player != playerController.Id)
            {
                OnTakeDamage?.Invoke(damage, player, throwable.gameObject.name);
            }
        }
        if (collision.collider.CompareTag("Bullet"))
        {
            AudioController.Instance.PlayRandomSoundAtPos(transform.position, SoundType.HIT);
            Vector3 position = collision.contacts[0].point;
            Quaternion rotation = Quaternion.LookRotation(collision.contacts[0].normal);
            ParticlesManager.Instance.PlayGunHitParticles(position, rotation, playerController.CharacterColor);
            Bullet bullet = collision.collider.GetComponentInParent<Bullet>();
            int damage = bullet.Damage;
            if (damage > 0)
            {
                OnTakeDamage?.Invoke(damage, bullet.shooterId, bullet.OriginGun.name);
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Fist"))
        {
            var fist = other.GetComponent<PlayerFist>();
            fist.PunchReceived();
            OnTakeDamage?.Invoke(3, fist.GetOwnerID(), "fist");
            AudioController.Instance.PlayRandomSoundAtPos(transform.position, SoundType.HIT);
        }
    }
}
