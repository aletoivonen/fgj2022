/*
Copyright (c) 2019 Juho Turpeinen
Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:
The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

using System.Collections.Generic;
using UnityEngine;

namespace ObjectPool
{
    public interface IPoolable
    {
        void OnInstantiated();
        void OnDestroyed();
        void OnPopulated();
    }

    public class Pool
    {
        private Dictionary<Object, Stack<Object>> pools;
        private Dictionary<Object, Object> instantiatedObjects;

        private bool TryCallOnPopulated(Object obj)
        {
            if (obj is GameObject)
            {
                GameObject go = obj as GameObject;
                IPoolable poolable = go.GetComponent<IPoolable>();
                if (poolable != null)
                {
                    poolable.OnPopulated();
                    return true;
                }
            }
            else if (obj is IPoolable)
            {
                IPoolable poolable = obj as IPoolable;
                poolable.OnPopulated();
                return true;
            }
            return false;
        }

        private bool TryCallOnInstantiated(Object obj)
        {
            if (obj is GameObject)
            {
                GameObject go = obj as GameObject;
                IPoolable poolable = go.GetComponent<IPoolable>();
                if (poolable != null)
                {
                    poolable.OnInstantiated();
                    return true;
                }
            }
            else if (obj is IPoolable)
            {
                IPoolable poolable = obj as IPoolable;
                poolable.OnInstantiated();
                return true;
            }
            return false;
        }

        private bool TryCallOnDestroyed(Object obj)
        {
            if (obj is GameObject)
            {
                GameObject go = obj as GameObject;
                IPoolable poolable = go.GetComponent<IPoolable>();
                if (poolable != null)
                {
                    poolable.OnDestroyed();
                    return true;
                }
            }
            else if (obj is IPoolable)
            {
                IPoolable poolable = obj as IPoolable;
                poolable.OnDestroyed();
                return true;
            }
            return false;
        }

        public T Instantiate<T>(Object obj, bool shouldCheckCallbacks = true) where T : Object
        {
            if (obj == null)
            {
                Debug.LogError("Tried to instantiate null via pool");
                return null;
            }

            Object next = null;
            Stack<Object> pool;
            if (pools.TryGetValue(obj, out pool))
            {
                while (pool.Count > 0)
                {
                    next = pool.Pop();
                    if (next != null)
                    {
                        break;
                    }
                }
            }

            if (next == null)
            {
                next = Object.Instantiate(obj) as T;
                if (shouldCheckCallbacks)
                {
                    TryCallOnPopulated(next);
                }
            }

            if (shouldCheckCallbacks)
            {
                TryCallOnInstantiated(next);
            }

            instantiatedObjects.Add(next, obj);
            return next as T;
        }

        public T Instantiate<T>(Object obj, Transform parent, bool shouldCheckCallbacks = true) where T : Object
        {
            T next = Instantiate<T>(obj, shouldCheckCallbacks);
            if (next is GameObject)
            {
                GameObject go = next as GameObject;
                go.transform.SetParent(parent);
            }
            else if (next is Component)
            {
                Component component = next as Component;
                component.transform.SetParent(parent);
            }
            return next;
        }

        public T Instantiate<T>(Object obj, Vector3 position, Quaternion rotation, bool shouldCheckCallbacks = true) where T : Object
        {
            T next = Instantiate<T>(obj, shouldCheckCallbacks);
            Transform transform = null;
            if (next is GameObject)
            {
                GameObject go = next as GameObject;
                transform = go.transform;
            }
            else if (next is Component)
            {
                Component component = next as Component;
                transform = component.transform;
            }
            if (transform != null)
            {
                transform.position = position;
                transform.rotation = rotation;
            }
            return next;
        }

        public T Instantiate<T>(Object obj, Vector3 position, Quaternion rotation, Transform parent, bool shouldCheckCallbacks = true) where T : Object
        {
            T next = Instantiate<T>(obj, parent, shouldCheckCallbacks);
            Transform transform = null;
            if (next is GameObject)
            {
                GameObject go = next as GameObject;
                transform = go.transform;
            }
            else if (next is Component)
            {
                Component component = next as Component;
                transform = component.transform;
            }
            if (transform != null)
            {
                transform.position = position;
                transform.rotation = rotation;
            }
            return next;
        }

        public void Destroy(Object obj, bool shouldCheckCallbacks = true)
        {
            if (obj == null)
            {
                Debug.LogError("Tried to destroy null via pool");
                return;
            }

            if (shouldCheckCallbacks)
            {
                TryCallOnDestroyed(obj);
            }

            Object original;
            if (instantiatedObjects.TryGetValue(obj, out original))
            {
                Stack<Object> pool;
                if (pools.TryGetValue(original, out pool))
                {
                    pool.Push(obj);
                    instantiatedObjects.Remove(obj);
                }
                else
                {
                    Debug.LogError("Tried to destroy object but no pool was found! Are you sure object was pooled?");
                }
            }
        }

        public void PopulatePool<T>(T obj, int count, bool shouldCheckCallbacks = true) where T : Object
        {
            if (obj == null)
            {
                Debug.LogError("Tried to populate pool with null object!");
                return;
            }

            Stack<Object> pool;
            pools.TryGetValue(obj, out pool);
            if (pool == null)
            {
                pool = new Stack<Object>();
                pools.Add(obj, pool);
            }
            for (int i = 0; i < count; i++)
            {
                T next = Object.Instantiate(obj);

                if (shouldCheckCallbacks)
                {
                    TryCallOnPopulated(next);
                }

                pool.Push(next);
            }
        }

        public void PopulatePool<T>(T obj, int count, Transform parent, bool shouldCheckCallbacks = true) where T : Object
        {
            if (obj == null)
            {
                Debug.LogError("Tried to populate pool with null object!");
                return;
            }

            Stack<Object> pool;
            pools.TryGetValue(obj, out pool);
            if (pool == null)
            {
                pool = new Stack<Object>();
                pools.Add(obj, pool);
            }
            for (int i = 0; i < count; i++)
            {
                T next = Object.Instantiate(obj, parent);

                if (shouldCheckCallbacks)
                {
                    TryCallOnPopulated(next);
                }

                pool.Push(next);
            }
        }

        public void ClearPool(Object obj, bool destroyContents = true)
        {
            if (obj == null)
            {
                return;
            }

            Stack<Object> pool;
            if (pools.TryGetValue(obj, out pool))
            {
                if (destroyContents)
                {
                    while (pool.Count > 0)
                    {
                        Object next = pool.Pop();
                        if (next != null)
                        {
                            if (next is Component)
                            {
                                Component component = next as Component;
                                Object.Destroy(component.gameObject);
                            }
                            else
                            {
                                Object.Destroy(next);
                            }
                        }
                    }
                }
                pools.Remove(obj);
            }
        }

        public void ClearAllPools(bool destroyContents = true)
        {
            Object[] keys = new Object[pools.Keys.Count];
            pools.Keys.CopyTo(keys, 0);
            foreach (Object key in keys)
            {
                ClearPool(key, destroyContents);
            }
        }

        public void DestroyInstantiatedObjects()
        {
            Object[] keys = new Object[instantiatedObjects.Keys.Count];
            instantiatedObjects.Keys.CopyTo(keys, 0);
            foreach (Object obj in keys)
            {
                if (obj == null)
                {
                    continue;
                }
                Destroy(obj);
            }
        }

        public T[] GetInstantiatedObjectsOfType<T>() where T : Object
        {
            List<T> ret = new List<T>();
            foreach (Object obj in instantiatedObjects.Keys)
            {
                if (instantiatedObjects[obj] is T)
                {
                    ret.Add(obj as T);
                }
            }
            return ret.ToArray();
        }

        public Pool()
        {
            pools = new Dictionary<Object, Stack<Object>>();
            instantiatedObjects = new Dictionary<Object, Object>();
        }
    }
}