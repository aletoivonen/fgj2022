using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerThrow : MonoBehaviour
{
    [SerializeField]
    private Transform leftThrowableTarget;
    [SerializeField]
    private Transform rightThrowableTarget;

    [SerializeField]
    private Transform throwDirectionTarget;

    [SerializeField]
    private float throwLength = 1.0f;

    [SerializeField]
    private AnimationCurve grabWeightSpeedRatio;

    [SerializeField]
    private AnimationCurve weightThrowForceRatio;

    private PlayerInputReceiver playerInput;

    private float throwTLeft;
    private float throwTRight;

    private Throwable throwableLeft;
    private Throwable throwableRight;

    private int directionLeft;
    private int directionRight;

    [SerializeField]
    private float maxTargetDist = 2.0f;

    private bool lastGrabLeft = false;
    private bool lastGrabRight = false;

    private Animator charAnimator;

    private PlayerController playerController;

    private void Start()
    {
        playerController = GetComponent<PlayerController>();
        playerInput = GetComponent<PlayerInputReceiver>();
        charAnimator = GetComponentInChildren<Animator>();
    }

    private void Update()
    {
        bool grabStartedLeft = !lastGrabLeft && playerInput.GrabInputLeft;
        bool grabStartedRight = !lastGrabRight && playerInput.GrabInputRight;
        lastGrabLeft = playerInput.GrabInputLeft;
        lastGrabRight = playerInput.GrabInputRight;

        if (throwableLeft == null && grabStartedLeft)
        {
            TryStartGrab(-1);
        }
        if (throwableRight == null && grabStartedRight)
        {
            TryStartGrab(1);
        }

        if (!playerInput.GrabInputLeft && throwableLeft != null)
        {
            DetachLeft();
        }

        if (!playerInput.GrabInputRight && throwableRight != null)
        {
            DetachRight();
        }

        directionLeft = throwableLeft != null ? 1 : -1;
        directionRight = throwableRight != null ? 1 : -1;
        LerpToDirection();
    }

    public bool CompareToTargets(GameObject go)
    {
        return go == throwableLeft || go == throwableRight;
    }

    public void GetLiftingStatus(out int left, out int right)
    {
        left = directionLeft;
        right = directionRight;
    }

    private void DetachLeft()
    {
        throwableLeft.Detach();
        Vector3 impulse = (throwDirectionTarget.position - transform.position) * weightThrowForceRatio.Evaluate(throwableLeft.GetMass() *
           GameController.Instance.ArmsStrengthMultiplier.Evaluate(playerController.NormalizedArmSize));
        if (throwableLeft == throwableRight)
        {
            impulse *= 1.5f;
            throwableRight = null;
        }
        throwableLeft.Impulse(impulse);
        throwableLeft = null;
    }

    private void DetachRight()
    {
        throwableRight.Detach();
        Vector3 impulse = (throwDirectionTarget.position - transform.position) * weightThrowForceRatio.Evaluate(throwableRight.GetMass() *
           GameController.Instance.ArmsStrengthMultiplier.Evaluate(playerController.NormalizedArmSize));
        if (throwableRight == throwableLeft)
        {
            impulse *= 1.5f;
            throwableLeft = null;
        }
        throwableRight.Impulse(impulse);
        throwableRight = null;
    }

    public void ForceRelease()
    {
        if (throwableLeft) DetachLeft();
        if (throwableRight) DetachRight();
    }

    private void LerpToDirection()
    {
        throwTLeft += directionLeft * (throwableLeft != null ? grabWeightSpeedRatio.Evaluate(throwableLeft.GetMass()) : 1) * Time.deltaTime;
        throwTLeft = Mathf.Clamp01(throwTLeft);
        charAnimator.SetFloat("LeftLift", throwTLeft * (throwableLeft != null && throwableLeft.IsGun ? 0.7f : 1.0f));

        throwTRight += directionRight * (throwableRight != null ? grabWeightSpeedRatio.Evaluate(throwableRight.GetMass()) : 1) * Time.deltaTime;
        throwTRight = Mathf.Clamp01(throwTRight);
        charAnimator.SetFloat("RightLift", throwTRight * (throwableRight != null && throwableRight.IsGun ? 0.7f : 1.0f));
    }

    private void TryStartGrab(int side)
    {
        var newThrowable = ThrowableController.Instance.GetClosest((side < 0 ? leftThrowableTarget : rightThrowableTarget).position, maxTargetDist);
        if (newThrowable != null)
        {
            if (side < 0) throwableLeft = newThrowable;
            else throwableRight = newThrowable;
            newThrowable.Attach(side < 0 ? leftThrowableTarget : rightThrowableTarget, playerController);
        }
    }

}
