using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerInputReceiver : MonoBehaviour
{
    public Vector2 MoveInput = Vector2.zero;
    public Vector2 LookInput = Vector2.zero;
    public bool JumpInput;

    public bool GrabInputLeft;
    public bool GrabInputRight;

    public bool PunchInputLeft;
    public bool PunchInputRight;
    private bool punchInputLeftLast;
    private bool punchInputRightLast;

#if ENABLE_INPUT_SYSTEM
    public void OnMove(InputValue value)
    {
        MoveInput = value.Get<Vector2>();
    }

    public void OnLook(InputValue value)
    {
        LookInput = value.Get<Vector2>();
    }

    public void OnJump(InputValue value)
    {
        JumpInput = value.isPressed;
    }

    public void OnGrabLeft(InputValue value)
    {
        GrabInputLeft = value.isPressed;
    }

    public void OnGrabRight(InputValue value)
    {
        GrabInputRight = value.isPressed;
    }

    public void OnPunchLeft(InputValue value)
    {
        PunchInputLeft = !punchInputLeftLast && value.isPressed;
        punchInputLeftLast = value.isPressed;
    }

    public void OnPunchRight(InputValue value)
    {
        PunchInputRight = !punchInputRightLast && value.isPressed;
        punchInputRightLast = value.isPressed;
    }
#endif

    private void Update()
    {
        JumpInput = false;
        PunchInputLeft = false;
        PunchInputRight = false;
    }

}